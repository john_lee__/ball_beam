ball_beam
=========

Ball and Beam Balancing Project on Beaglebone Black

instructions for use:

type the following into your console (terraterm):

git clone https://github.com/john-s-lee/ball_beam.git 

cd ball_beam 

make


Then test by attaching the servo signal wire (yellow) into P8_13. Attach servo ground to Beaglebone ground.  Atach Servo Power supply positive wire to the red wire on the servo and the negative wire to a ground on the beaglebone.

See if program works by typing the following into the console:

./project \<angle\>

where <angle> is the angle to drive the servo to in degrees. May be positive or negative.

e.g.   ./project -30

Shound drive the servo to -ve 30 degrees.

I have hacked up the ADAFRUIT python IO library in order to make the servo very simple to use.
I will help you do the same for your sensor once you have it and know how it works.
Your main source files to work on will be project.c and project.h which is in the source folder.  The other files in there are the modified ADAFRUIT ones which you shouldn't really need to use.  There is also a makefile included in the git so all you need to do to compile is type "make" without the quotation marks from the ball_beam directory.  

Cheers,

John.


