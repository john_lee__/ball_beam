#define PWM_REQUIRED "P8_13"  //Which pin should PWM be enable for?
#define SERVO_FREQUENCY 50 //Servo refresh rate (normally 50 Hz)
#define SERVO_CENTRE 1500 //Duty period for servo centre position in us (normally 1500 us)

//  PID Parameters
#define TAUF 0.00	
#define TAUD 0.00
#define TAUI 0.00
#define KC 0.00


//  Discretization Parameters
#define DELTAT 20


