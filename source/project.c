#include <stdio.h>
#include "c_pwm.h"
#include "project.h"


int main(int argc, char * argv[])
{
	if (argc != 2)
	{
		printf("Correct usage: ./project <angle>\n");
		printf("Where <angle> is the required servo angle in degrees\n");
		printf("e.g. ./project 30   or ./project -30\n");
		return 0;
	}


	float required_angle = atof(argv[1]);

        // Start the PWM with frequency of servo and drive servo to required position
	if (pwm_start("P8_13", get_duty_percentage(required_angle), SERVO_FREQUENCY) < 0)
	{
		printf("Unable to initialise PWM!!!\n");
		return 0;
	}

	//Once PWM has been started as above future driving is done by the drive_servo() function
	//  e.g. to drive servo to 10.2 degrees code required is:   drive_servo(10.2)

	return 0;
}


